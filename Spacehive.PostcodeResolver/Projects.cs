﻿namespace Spacehive.PostcodeResolver
{
    partial class Program
    {
        public class Projects
        {
            public int Id { get; set; }

            public double Latitude { get; set; }

            public double Longitude { get; set; }

            public string PostCode { get; set; }
        }
    }
}
