﻿using Dapper;
using Newtonsoft.Json.Linq;

using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Spacehive.PostcodeResolver
{
    partial class Program
    {
        public const string FileName = "myCsvFile";

        public const string ConnectionString = "Server=tcp:wbwool7lbd.database.windows.net,1433;Database=SpacehiveDevDB;User ID=SpacehiveDB@wbwool7lbd;Password=P4st1ng_1;Trusted_Connection=False;Encrypt=True;Connection Timeout=30;";

        public static void Main(string[] args)
        {
            Console.WriteLine("Spacehive.PostcodeResolver");
            Console.WriteLine("Attempting to resolve postcodes..");

            var sql = @"
select 
p.Id, 
p.Latitude, 
p.Longitude 
from Projects p
where 1=1 
and p.FundingStartDate is not null
order by p.Id asc
";
            var csvFileName = string.Format("{0}.csv", FileName);

            var csvSkipped = string.Format("{0}_skipped.csv", FileName);

            var projects = new List<Projects>();

            var skippedProjects = new List<Projects>();

            using (var sqlConnection = new SqlConnection(ConnectionString))
            {
                sqlConnection.Open();

                IEnumerable products = sqlConnection.Query(sql);

                foreach (dynamic product in products)
                {
                    // get the fields
                    var id = product.Id;
                    var lat = product.Latitude;
                    var lon = product.Longitude;

                    try
                    {
                        Task.Run(async () =>
                        {
                            var resolvedPostcode = await ResolvePostcodeAsync(lat.ToString(), lon.ToString());

                            // add good entries
                            projects.Add(new Projects()
                            {
                                Id = id,
                                Latitude = lat,
                                Longitude = lon,
                                PostCode = resolvedPostcode
                            });
                        }).GetAwaiter().GetResult();
                    }
                    catch (Exception ex)
                    {
                        // catch any bad entries
                        skippedProjects.Add(new Projects()
                        {
                            Id = id,
                            Latitude = lat,
                            Longitude = lon
                        });
                    }
                }
                
                // remove files if they exist
                if (File.Exists(csvFileName)) File.Delete(csvFileName);
                if (File.Exists(csvSkipped)) File.Delete(csvSkipped);

                // save file
                if (projects.Count > 0) File.WriteAllText(csvFileName, ProjectsToCsv(projects).ToString());
                if (skippedProjects.Count > 0) File.WriteAllText(csvSkipped, ProjectsToCsv(skippedProjects).ToString());
            }


            Console.WriteLine("{0} postcodes successfully resolved from coordinates", projects.Count);
            Console.WriteLine("{0} postcodes skipped", skippedProjects.Count);
            Console.WriteLine("Press the enter key to exit");
            Console.ReadLine();
        }

        public static async Task<string> ResolvePostcodeAsync(string Latitude, string Longitude)
        {
            string ping = string.Format("http://api.postcodes.io/postcodes?lat={0}&lon={1}", Latitude.ToString(), Longitude.ToString());
            
            using (HttpClient client = new HttpClient())
            {
                client.BaseAddress = new Uri(ping);

                HttpResponseMessage response = await client.GetAsync(ping);

                var content = await response.Content.ReadAsStringAsync();

                dynamic data = JObject.Parse(content.ToString());

                var result = data["result"];

                // throw if no results
                if (result == null) throw new Exception("No results for this coordinate");

                // return the postcode
                return result[0]["postcode"].Value;
            }
        }

        public static string ProjectsToCsv(List<Projects> Projects)
        {
            var stringToReturn = new StringBuilder();

            foreach (var p in Projects)
            {
                stringToReturn.AppendLine(string.Format("{0},{1},{2},{3}", p.Id, p.Latitude, p.Longitude, p.PostCode));
            }

            return stringToReturn.ToString();
        }
    }
}
